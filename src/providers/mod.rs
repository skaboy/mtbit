pub mod mtbmag;
use snafu::Snafu;
use crate::tracks;
use snafu::ResultExt;
use serde::Serialize;
use std::fs::File;
use std::io::{BufWriter, BufReader};
use serde::de::DeserializeOwned;
use crate::providers::mtbmag::MtbMag;
use std::collections::HashMap;

#[derive(Debug, Snafu)]
pub enum Error {
    #[snafu(display("Impossible to login to mtbmag: {}", source))]
    NoLogin {
        source: reqwest::Error,
    },

    #[snafu(display("Impossible to login to mtbmag"))]
    FailLogin { },

    #[snafu(display("Impossible to cache"))]
    NoCache { },

    #[snafu(display("No provider"))]
    NoProvider { },

    #[snafu(display("Serialize/Deserialize provider file error"))]
    DeSeFile { source: std::io::Error },

    #[snafu(display("Serialize/Deserialize provider error"))]
    DeSe { source: serde_json::error::Error },

    #[snafu(display("No GPX file"))]
    Empty {},
}

type Result<T, E = Error> = std::result::Result<T, E>;

pub trait ProviderConstructor {
    type Options: Serialize + DeserializeOwned;

    fn new(options: Self::Options) -> Result<Self> where Self: Sized;
    fn get_options(&self) -> &Self::Options;
    fn get_name(&self) -> &str;

    fn save(&self) -> Result<()> {
        let dir = xdg::BaseDirectories::with_prefix("mtbit").unwrap();
        let f = File::create(dir.place_data_file(self.get_name()).unwrap())
            .context(DeSeFile {})?;
        let writer = BufWriter::new(f);
        serde_json::to_writer_pretty(writer, self.get_options())
            .context(DeSe {})?;
        Ok(())
    }

    fn load(name: &str) -> Result<Self> where Self: Sized {
        let dir = xdg::BaseDirectories::with_prefix("mtbit").unwrap();
        let opts: Self::Options;
        if let Some(p) = dir.find_data_file(name) {
            let f = File::open(p).expect("Impossible to open provider file");
            let reader = BufReader::new(f);
            opts = serde_json::from_reader(reader).expect("Impossible to parse provider file");
            Self::new(opts)
        }else {
            NoProvider {}.fail()
        }
    }
}

pub trait Provider {
    fn parse_index(&self) -> Result<Vec<tracks::Track>>;
    fn download(&self, key: String) -> Result<()>;
}

pub fn exec(name: &str, cache: &mut HashMap<String, Box<dyn Provider>>, c: impl Fn(&Box<dyn Provider>)) -> Result<()> {
    let p = if let Some(a) = cache.get(name) {
        a
    }else {
        match name {
            "mtbmag" => {
                let p = MtbMag::load("mtbmag")?;
                let b: Box<dyn Provider> = Box::new(p);
                cache.insert(name.into(), b);
                &cache[name]
            },
            _ => NoProvider {}.fail()?
        }
    };
    Ok(c(p))
}
