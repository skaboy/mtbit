use reqwest::Client;
use std::time::Duration;
use std::io::BufWriter;
use std::fs::File;
use std::fs;
use serde::{Deserialize, Serialize};
use snafu::ResultExt;
use crate::tracks;
use crate::providers;

const ALL_JSON: &str = "all.json";

type Result<T, E = providers::Error> = std::result::Result<T, E>;

#[derive(Deserialize, Serialize)]
pub struct MtbMagOptions {
    pub name: String,
    pub username: String,
    pub password: String
}

pub struct MtbMag {
    client: Client,
    pub options: MtbMagOptions
}

#[derive(Deserialize, Debug)]
struct User {
    name: String,
    id: String
}

#[derive(Deserialize, Debug)]
struct Category {

}

#[derive(Deserialize, Debug)]
struct Tour {
    id: String,
    title: String,
    latitude: String,
    longitude: String
}

#[derive(Deserialize, Debug)]
struct Track {
    #[serde(rename = "User")]
    user: User,
    #[serde(rename = "Category")]
    cat: Category,
    #[serde(rename = "Tour")]
    tour: Tour
}

impl MtbMag {
    fn download_index(&self) -> Result<()> {
        let dir = xdg::BaseDirectories::with_prefix("mtbit").unwrap();
        if dir.find_cache_file(ALL_JSON).is_some() {
            return Ok(());
        }
        let f = File::create(dir.place_cache_file(ALL_JSON).unwrap()).expect("Impossible to cache file");
        let mut writer = BufWriter::new(f);

        let _all = self.client.get("https://itinerari.mtb-mag.com/all.json")
            .header("X-Requested-With", "XMLHttpRequest")
            .send()
            .context(providers::NoLogin {})?
            .copy_to(&mut writer)
            .context(providers::NoLogin {})?;

        Ok(())
    }
}

impl providers::ProviderConstructor for MtbMag {
    type Options = MtbMagOptions;

    fn new(options: Self::Options) -> Result<Self> {
        let client = reqwest::Client::builder()
            .timeout(Duration::from_secs(10))
            .cookie_store(true)
            .redirect(reqwest::RedirectPolicy::none())
            .build()
            .expect("Impossible to initialize reqwest library");

        let params = [("login", &options.username),
            ("password", &options.password),
            ("remember", &String::from("1"))];
        let res = client.post("https://www.mtb-mag.com/forum/login/login")
            .form(&params)
            .send()
            .context(providers::NoLogin { })?;
        if res.status() == 303 {
            println!("success");
        } else {
            println!("error: {:?}", res.status());
            return providers::FailLogin { }.fail();
        }
        Ok(MtbMag{ client, options })
    }

    fn get_options(&self) -> &Self::Options {
        &self.options
    }

    fn get_name(&self) -> &str {
        &self.options.name
    }
}

impl providers::Provider for MtbMag {
    fn parse_index(&self) -> Result<(Vec<tracks::Track>)> {
        self.download_index()?;
        let dir = xdg::BaseDirectories::with_prefix("mtbit").unwrap();
        let data = &fs::read(dir.find_cache_file(ALL_JSON).unwrap()).unwrap();
        let tracks = serde_json::from_slice::<Vec<Track>>(&data).expect("Error parsing file");
        let mut ttracks: Vec<tracks::Track> = Vec::new();
        for t in tracks {
            println!("Track: {} -> {}", t.tour.id, t.tour.title);
            ttracks.push(tracks::Track::new(t.tour.title.clone(),
                                            t.tour.latitude.parse::<f32>().unwrap_or(0.0),
                                            t.tour.longitude.parse::<f32>().unwrap_or(0.0),
                                            String::from("mtbmag"),
                                            t.tour.id.clone()))
        }
        Ok(ttracks)
    }

    fn download(&self, key: String) -> Result<()> {
        let dir = xdg::BaseDirectories::with_prefix("mtbit").unwrap();
        let fname = format!("gpx/mtbmag-{}.gpx", key);

        let url = format!("https://itinerari.mtb-mag.com/tours/track/{}/type:gpx", key);
        if let Ok(req) = self.client.get(&url).build() {
            let path = dir.place_cache_file(fname).unwrap();
            let f = File::create(path.clone()).expect("Impossible to create gpx file");
            let mut writer = BufWriter::new(f);
            let num = self.client.execute(req)
                .context(providers::NoLogin {})?
                .copy_to(&mut writer)
                .context(providers::NoLogin {})?;
            if num == 0 {
                fs::remove_file(path).unwrap();
                return providers::Empty {}.fail();
            }
        }
        Ok(())
    }
}