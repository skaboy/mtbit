// * mtbmag

// concepts:
//  * providers (directory, mtbmag, locus...)
//  * tracks: list of tracks from providers

// providers:
//  * define providers (data, login, cache, ...)
//  * sync

// tracks:
//  * filter / list
//  * download
//  * work only on downloaded tracks
//  * get statistics


// FIXME:
// * tracks
// - load gpx from different directories
// - create cache from gpx files for faster filters
// - cache statistics (len, time, D+) for all tracks (https://towardsdatascience.com/how-tracking-apps-analyse-your-gps-data-a-hands-on-tutorial-in-python-756d4db6715d)
// - filter on statistics
// - see details for tracks
// - track-view graphical with tiles

// * providers
// - add `directory` provider
// - add `locusdb` provider
// - add sync command (how to sync/update index data?)

// * general
// - tests
// - gitlab project public
// - error reporting / testing
// - async (reqwest, output, files, processing tracks)
// - display? or open another app? https://wiki.openstreetmap.org/wiki/Frameworks
mod providers;
mod tracks;

use structopt::StructOpt;
use providers::{Provider, ProviderConstructor};
use snafu::Snafu;
use snafu::ResultExt;
use providers::mtbmag::{MtbMag, MtbMagOptions};
use std::collections::HashMap;

#[derive(Debug, Snafu)]
pub enum Error {
    #[snafu(display("Error in tracks"))]
    TracksError {
        source: tracks::Error
    },
    #[snafu(display("Error in providers"))]
    ProviderError {
        source: providers::Error
    },
    #[snafu(display("Error in providers 2"))]
    ProviderErrorCache { }
}

type Result<T, E = Error> = std::result::Result<T, E>;

#[derive(Debug, StructOpt)]
#[structopt(name = "mtbit", about="mtbit")]
enum Opt {
    MtbMag {
        #[structopt(long)]
        username: String,
        #[structopt(long)]
        password: String,
    },
    TracksFilter {
        #[structopt(long)]
        address: String,
        #[structopt(long)]
        distance: u32,
        #[structopt(long)]
        download: bool,
    },
    TracksList {
    }
}

fn main() -> Result<()> {
    let opt = Opt::from_args();
    let mut providers_cache: HashMap<String, Box<dyn providers::Provider>> = HashMap::new();
    match opt {
        Opt::MtbMag { username, password } => {
            let mut tt = tracks::Tracks::new();
            let p = MtbMag::new(MtbMagOptions{
                name: String::from("mtbmag"),
                username, password}).context(ProviderError {})?;
            p.save().context(ProviderError {})?;
            let tracks = p.parse_index().context(ProviderError {})?;
            tt.add(tracks);
            tt.save().context(TracksError {})?;
        },
        Opt::TracksFilter { address, distance, download} => {
            let tt = tracks::Tracks::new();
            for t in tt.filter_address(&address, distance) {
                println!("{:?}", t);
                if download {
                    providers::exec(&t.provider, &mut providers_cache,
                                    |p| {
                                        if p.download(t.id.clone()).is_err() {
                                            println!("Error downloading {:?}", t);
                                        }
                                    }).context(ProviderError {})?;
                }
            }
        },
        Opt::TracksList { } => {
            let tt = tracks::Tracks::new();
            tt.list();
        }
    }
    Ok(())
}
