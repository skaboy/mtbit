use snafu::Snafu;
use snafu::ResultExt;
use serde::{Deserialize, Serialize};
use std::io::{BufWriter, BufReader};
use std::fs::File;
use geo::Point;
use geo::prelude::*;
use geocoding::openstreetmap::Openstreetmap;
use geocoding::Forward;
use std::collections::HashMap;

const DB_JSON: &str = "tracks.json";

#[derive(Debug, Snafu)]
pub enum Error {
    #[snafu(display("Impossible to cache file"))]
    FailDB {
        source: serde_json::error::Error
    },
    #[snafu(display("Impossible to cache db file"))]
    FailSaveDB {
        source: std::io::Error
    },

}

type Result<T, E = Error> = std::result::Result<T, E>;

#[derive(Deserialize, Debug, Serialize)]
pub struct Track {
    name: String,
    lat: f32,
    lon: f32,
    pub provider: String,
    pub id: String
}

impl Track {
    pub fn new(name: String, lat: f32, lon: f32, provider: String, id: String) -> Track {
        Track { name, lat, lon, provider, id}
    }

    pub fn get_key(&self) -> String {
        format!("{}-{}", self.provider, self.id)
    }
}

pub struct Tracks {
    tt: HashMap<String, Track>,
}

impl Tracks {
    pub fn new() -> Self {
        let dir = xdg::BaseDirectories::with_prefix("mtbit").unwrap();
        let tt: HashMap<String, Track>;
        if let Some(p) = dir.find_cache_file(DB_JSON) {
            let f = File::open(p).expect("Impossible to open cache file");
            let reader = BufReader::new(f);
            tt = serde_json::from_reader(reader).expect("Impossible to parse db");
        } else {
            tt = HashMap::new();
        }
        Tracks { tt }
    }

    pub fn filter_address(&self, address: &str, distance: u32) -> Vec<&Track> {
        let osm = Openstreetmap::new();
        if let Ok(res) = osm.forward(address) {
            return self.filter(res[0].lng(), res[0].lat(), distance);
        }
        Vec::new()
    }

    pub fn filter(&self, lon: f32, lat: f32, distance: u32) -> Vec<&Track> {
        let p = Point::<f32>::from((lon, lat));
        let mut tt = Vec::new();
        for t in self.tt.values() {
            let dist = p.vincenty_distance(&Point::<f32>::from((t.lon, t.lat))).unwrap();
            if dist < distance as f32 {
                tt.push(t);
            }
        }
        tt
    }

    pub fn list(&self) {
        for (key, t) in &self.tt {
            println!("{} - {}", key, t.name);
        }
    }

    pub fn add(&mut self, tt: Vec<Track>) {
        for t in tt {
            let key = t.get_key();
            self.tt.insert(key, t);
        }
    }

    pub fn save(&self) -> Result<()> {
        let dir = xdg::BaseDirectories::with_prefix("mtbit").unwrap();
        let f = File::create(dir.place_cache_file(DB_JSON).unwrap())
            .context(FailSaveDB {})?;
        let writer = BufWriter::new(f);
        serde_json::to_writer_pretty(writer, &self.tt)
            .context(FailDB {})?;
        Ok(())
    }
}